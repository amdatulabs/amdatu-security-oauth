/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.oauth.impl;

import static org.amdatu.security.oauth.OAuthConstants.OAUTH_API_PROVIDER;
import static org.amdatu.security.oauth.OAuthConstants.OAUTH_API_VERSION;
import static org.amdatu.security.oauth.impl.provider.DefaultApiProviders.DEFAULT_APIS;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;
import org.scribe.builder.api.Api;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Activator extends DependencyActivatorBase {
    private static final String PID = "org.amdatu.security.oauth.service.factory";

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        // Register all known API providers by default...
        registerDefaultApiProviders(context, manager);

        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, PID);

        manager.add(createComponent()
            .setInterface(ManagedServiceFactory.class.getName(), props)
            .setImplementation(OAuthProviderFactory.class)
            .add(createServiceDependency().setService(LogService.class).setRequired(false)));
    }

    private void registerDefaultApiProviders(BundleContext context, DependencyManager manager) throws Exception {
        Bundle b = context.getBundle();
        for (Object[] apiProps : DEFAULT_APIS) {
            Api api = (Api) b.loadClass((String) apiProps[0]).newInstance();

            manager.add(createComponent()
                .setInterface(Api.class.getName(), createApiProps(apiProps))
                .setImplementation(api));
        }
    }

    private Properties createApiProps(Object[] props) {
        Properties result = new Properties();
        result.put(OAUTH_API_PROVIDER, props[1]);
        result.put(OAUTH_API_VERSION, props[2]);
        return result;
    }
}
