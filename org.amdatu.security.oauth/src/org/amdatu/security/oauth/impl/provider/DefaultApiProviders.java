/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.oauth.impl.provider;

import static org.amdatu.security.oauth.OAuthConstants.OAUTH_V1;
import static org.amdatu.security.oauth.OAuthConstants.OAUTH_V2;

import org.scribe.builder.api.Api;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface DefaultApiProviders {
    /**
     * Array of all default API providers. Each provider is an array of three elements:
     * <ol>
     * <li>the class name of the {@link Api} subclass;
     * <li>the name of the provider (to reference it from the service registry);
     * <li>the OAuth version, either OAUTH_V1 or OAUTH_V2.
     * </ol>
     */
    Object[][] DEFAULT_APIS = {
        // OAuth API providers...
        { "org.scribe.builder.api.AWeberApi", "AWeber", OAUTH_V1 },
        { "org.scribe.builder.api.ConstantContactApi", "ConstantContact", OAUTH_V1 },
        { "org.scribe.builder.api.DiggApi", "Digg", OAUTH_V1 },
        { "org.scribe.builder.api.DropBoxApi", "DropBox", OAUTH_V1 },
        { "org.scribe.builder.api.EvernoteApi", "Evernote", OAUTH_V1 },
        { "org.scribe.builder.api.FlickrApi", "Flickr", OAUTH_V1 },
        { "org.scribe.builder.api.FoursquareApi", "Foursquare", OAUTH_V1 },
        { "org.scribe.builder.api.FreelancerApi", "Freelancer", OAUTH_V1 },
        { "org.scribe.builder.api.GetGlueApi", "GetGlue", OAUTH_V1 },
        { "org.scribe.builder.api.GoogleApi", "Google", OAUTH_V1 },
        { "org.scribe.builder.api.ImgUrApi", "ImgUr", OAUTH_V1 },
        { "org.scribe.builder.api.KaixinApi", "Kaixin", OAUTH_V1 },
        { "org.scribe.builder.api.LinkedInApi", "LinkedIn", OAUTH_V1 },
        { "org.scribe.builder.api.LoveFilmApi", "LoveFilm", OAUTH_V1 },
        { "org.scribe.builder.api.MeetupApi", "Meetup", OAUTH_V1 },
        { "org.scribe.builder.api.MendeleyApi", "Mendeley", OAUTH_V1 },
        { "org.scribe.builder.api.MisoApi", "Miso", OAUTH_V1 },
        { "org.scribe.builder.api.NeteaseWeibooApi", "NeteaseWeiboo", OAUTH_V1 },
        { "org.scribe.builder.api.NetProspexApi", "NetProspex", OAUTH_V1 },
        { "org.scribe.builder.api.PlurkApi", "Plurk", OAUTH_V1 },
        { "org.scribe.builder.api.Px500Api", "Px500", OAUTH_V1 },
        { "org.scribe.builder.api.QWeiboApi", "QWeibo", OAUTH_V1 },
        { "org.scribe.builder.api.SapoApi", "Sapo", OAUTH_V1 },
        { "org.scribe.builder.api.SimpleGeoApi", "SimpleGeo", OAUTH_V1 },
        { "org.scribe.builder.api.SinaWeiboApi", "SinaWeibo", OAUTH_V1 },
        { "org.scribe.builder.api.SkyrockApi", "Skyrock", OAUTH_V1 },
        { "org.scribe.builder.api.SohuWeiboApi", "SohuWeibo", OAUTH_V1 },
        { "org.scribe.builder.api.TrelloApi", "Trello", OAUTH_V1 },
        { "org.scribe.builder.api.TumblrApi", "Tumblr", OAUTH_V1 },
        { "org.scribe.builder.api.TwitterApi", "Twitter", OAUTH_V1 },
        { "org.scribe.builder.api.UbuntuOneApi", "UbuntuOne", OAUTH_V1 },
        { "org.scribe.builder.api.VimeoApi", "Vimeo", OAUTH_V1 },
        { "org.scribe.builder.api.XingApi", "Xing", OAUTH_V1 },
        { "org.scribe.builder.api.YahooApi", "Yahoo", OAUTH_V1 },
        // OAuth2 API providers...
        { "org.scribe.builder.api.ConstantContactApi2", "ConstantContact", OAUTH_V2 },
        { "org.scribe.builder.api.FacebookApi", "Facebook", OAUTH_V2 },
        { "org.scribe.builder.api.Foursquare2Api", "Foursquare", OAUTH_V2 },
        { "org.scribe.builder.api.GoogleApi20", "Google", OAUTH_V2 },
        { "org.scribe.builder.api.KaixinApi20", "Kaixin", OAUTH_V2 },
        { "org.scribe.builder.api.LiveApi", "Live", OAUTH_V2 },
        { "org.scribe.builder.api.RenrenApi", "Renren", OAUTH_V2 },
        { "org.scribe.builder.api.SinaWeiboApi20", "SinaWeibo", OAUTH_V2 },
        { "org.scribe.builder.api.ViadeoApi", "Viadeo", OAUTH_V2 },
        { "org.scribe.builder.api.VkontakteApi", "Vkontakte", OAUTH_V2 },
    };
}
