/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.oauth.impl;

import static org.amdatu.security.oauth.OAuthConstants.*;

import java.io.OutputStream;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;
import org.scribe.builder.api.Api;
import org.scribe.model.OAuthConfig;
import org.scribe.model.OAuthConstants;
import org.scribe.model.SignatureType;
import org.scribe.oauth.OAuthService;

/**
 * Provides means to create new {@link OAuthService}s based on configurations.
 */
public class OAuthProviderFactory implements ManagedServiceFactory {
    // Injected by Felix DM...
    private volatile DependencyManager m_dm;
    private volatile BundleContext m_context;
    private volatile LogService m_log;
    // Locally managed...
    private final ConcurrentMap<String, Component> m_services = new ConcurrentHashMap<>();

    @Override
    public void deleted(String pid) {
        Component comp = m_services.remove(pid);
        if (comp != null) {
            m_log.log(LogService.LOG_INFO, "Unregistering OAuth service...");
            m_dm.remove(comp);
        }
    }

    @Override
    public String getName() {
        return "OAuth Service Provider Factory";
    }

    @Override
    public void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
        String apiProvider = getMandatoryString(properties, OAUTH_API_PROVIDER);
        String apiVersion = getMandatoryString(properties, OAUTH_API_VERSION);
        try {
            int v = Integer.valueOf(apiVersion);
            if (!OAUTH_V1.equals(v) && !OAUTH_V2.equals(v)) {
                throw new ConfigurationException(OAUTH_API_VERSION, "invalid value: should be either 1 or 2!");
            }
        }
        catch (NumberFormatException e) {
            throw new ConfigurationException(OAUTH_API_VERSION, "invalid numeric value!", e);
        }

        m_log.log(LogService.LOG_DEBUG, String.format("Creating new OAuth service for provider %s", apiProvider));

        Properties props = new Properties();
        props.put(OAUTH_API_PROVIDER, apiProvider);
        props.put(OAUTH_API_VERSION, apiVersion);

        OAuthService service = createOAuthService(apiProvider, apiVersion, properties);

        m_services.computeIfAbsent(pid, str -> {
            m_log.log(LogService.LOG_INFO, String.format("Registering OAuth service for provider %s", apiProvider));

            Component c = m_dm.createComponent()
                .setInterface(OAuthService.class.getName(), props)
                .setImplementation(service);
            m_dm.add(c);

            return c;
        });
    }

    private OAuthConfig createOAuthConfig(Dictionary<String, ?> properties) throws ConfigurationException {
        String apiKey = getMandatoryString(properties, OAUTH_API_KEY);
        String apiSecret = getMandatoryString(properties, OAUTH_API_SECRET);
        String callbackUrl = getString(properties, OAUTH_CALLBACK_URL);
        if (callbackUrl == null) {
            callbackUrl = OAuthConstants.OUT_OF_BAND;
        }

        String scope = getString(properties, OAUTH_SCOPE);
        String signatureType = getString(properties, OAUTH_SIGNATURE_TYPE);
        String grantType = getString(properties, OAUTH_GRANT_TYPE);
        String accessType = getString(properties, OAUTH_ACCESS_TYPE);
        String approvalPrompt = getString(properties, OAUTH_APPROVAL_PROMPT);

        SignatureType sigType = (signatureType == null) ? SignatureType.Header : SignatureType.valueOf(signatureType);
        OutputStream stream = null;

        return new OAuthConfig(apiKey, apiSecret, callbackUrl, sigType, grantType, accessType, scope, approvalPrompt, stream);
    }

    private OAuthService createOAuthService(String apiProvider, String apiVersion, Dictionary<String, ?> properties) throws ConfigurationException {
        Collection<ServiceReference<Api>> serviceRefs;

        try {
            String filter = String.format("(&(api.provider=%s)(api.version=%s))", apiProvider, apiVersion);
            serviceRefs = m_context.getServiceReferences(Api.class, filter);
        }
        catch (InvalidSyntaxException e) {
            throw new RuntimeException("Invalid syntax?!", e);
        }

        if (serviceRefs == null || serviceRefs.isEmpty()) {
            throw new ConfigurationException(OAUTH_API_PROVIDER, "no such provider: " + apiProvider);
        }

        ServiceReference<Api> serviceRef = serviceRefs.iterator().next();
        Api api = m_context.getService(serviceRef);
        try {
            if (api == null) {
                // Unfortunate timing issue: the service was deregistered after we've obtained the serviceRef...
                return null;
            }
            return api.createService(createOAuthConfig(properties));
        }
        finally {
            m_context.ungetService(serviceRef);
        }
    }

    private static String getMandatoryString(Dictionary<String, ?> properties, String key) throws ConfigurationException {
        String val = getString(properties, key);
        if (val == null) {
            throw new ConfigurationException(key, "missing or invalid value!");
        }
        return val;
    }

    private static String getString(Dictionary<String, ?> properties, String key) throws ConfigurationException {
        Object val = properties.get(key);
        if (val == null || "".equals(val)) {
            return null;
        }
        if (!(val instanceof String)) {
            throw new ConfigurationException(key, "invalid value: must be a string!");
        }
        return (String) val;
    }
}
