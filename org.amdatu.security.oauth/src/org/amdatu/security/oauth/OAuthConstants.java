/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.oauth;

/**
 * Common constants for the OAuth service providers.
 */
public interface OAuthConstants {
    /** The name of the OAuth provider, as lower case string. Examples are "google", "twitter", etc. */
    String OAUTH_API_PROVIDER = "api.provider";
    /** Whether a API provider implements OAuth v1 or v2. */
    String OAUTH_API_VERSION = "api.version";

    Integer OAUTH_V1 = Integer.valueOf(1);
    Integer OAUTH_V2 = Integer.valueOf(2);

    String OAUTH_API_KEY = "api.key";
    String OAUTH_API_SECRET = "api.secret";
    String OAUTH_CALLBACK_URL = "callback.url";
    String OAUTH_SCOPE = "scope";
    String OAUTH_SIGNATURE_TYPE = "signature.type";
    String OAUTH_GRANT_TYPE = "grant.type";
    String OAUTH_ACCESS_TYPE = "access.type";
    String OAUTH_APPROVAL_PROMPT = "approval.prompt";
    String OAUTH_DEBUG = "debug";
}
