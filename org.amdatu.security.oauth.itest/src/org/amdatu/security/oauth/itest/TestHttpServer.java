/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.oauth.itest;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

/**
 * Provides a minimal HTTP server implementation, useful for creating web-based tests.
 * <p>
 * This implementation uses a HTTP server implementation as present in the JDK, for it to work in
 * an OSGi environment you need to make sure that the package "com.sun.net.httpserver" is not
 * imported and that it is present on the boot-classpath. For example, add:<code>
 * 
 * <pre>
Import-Package: !com.sun.net.httpserver,*
-runproperties: org.osgi.framework.bootdelegation="sun.*,com.sun.*"
 * </pre>
 * 
 * </code>
 * to your Bnd run file.
 * </p>
 * <p>
 * Also be aware that this server is by no means intended for production means. In fact, it only
 * uses a single background thread for handling requests, making it less suitable for highly
 * concurrent scenarios.
 * </p>
 */
@SuppressWarnings("restriction")
public class TestHttpServer {
    /** Denotes the HTTP request information. */
    public static final class HttpRequest {
        private final String m_method;
        private final URI m_request;
        private final Map<String, String> m_query;
        private final Headers m_headers;

        HttpRequest(HttpExchange exchange) {
            m_method = exchange.getRequestMethod();
            m_request = exchange.getRequestURI();
            m_query = new HashMap<>();
            m_headers = exchange.getRequestHeaders();

            // Parse query...
            String query = m_request.getQuery();
            if (query != null) {
                for (String entry : query.split("[&;]")) {
                    String[] parts = entry.split("=");
                    m_query.put(parts[0], parts.length > 1 ? parts[1] : null);
                }
            }
        }

        public final Map<String, List<String>> getHeaders() {
            return m_headers;
        }

        public final String getMethod() {
            return m_method;
        }

        public final String getPath() {
            return m_request.getPath();
        }

        public final String getQueryParam(String key) {
            return m_query.get(key);
        }

        public final URI getRequest() {
            return m_request;
        }
    }

    /** Callback handler for handling HTTP requests. */
    public interface HttpRequestCallback {
        /**
         * Called for all incoming requests.
         * 
         * @param request the request information, never <code>null</code>.
         * @return the response, or <code>null</code> if the request could not be handled.
         */
        HttpResponse handle(HttpRequest request);
    }

    /** Denotes the response send back to the client. */
    public static final class HttpResponse {
        private final int m_code;
        private final String m_body;
        private final String m_contentType;

        public HttpResponse(int code) {
            this(code, "");
        }

        public HttpResponse(int code, String body) {
            this(code, body, "text/plain; charset=UTF-8");
        }

        public HttpResponse(int code, String body, String contentType) {
            m_code = code;
            m_body = body;
            m_contentType = contentType;
        }

        final void writeTo(HttpExchange exchange) throws IOException {
            byte[] body = m_body.getBytes("UTF-8");
            exchange.sendResponseHeaders(m_code, body.length);
            exchange.getResponseHeaders().add("Content-Type", m_contentType);
            try (OutputStream os = exchange.getResponseBody()) {
                os.write(body);
            }
        }
    }

    private static final HttpResponse RESPONSE_NOT_FOUND = new HttpResponse(404);
    private static final int DEFAULT_BACKLOG = 0;

    private final int m_port;
    private final HttpServer m_server;

    /**
     * Creates a new {@link TestHttpServer} instance for all incoming requests.
     * 
     * @param port the port number to listen to for incoming requests, &gt; 0;
     * @param callback the callback to invoke for each incoming request, cannot be <code>null</code>.
     * @throws IOException in case opening the server failed.
     */
    public TestHttpServer(int port, HttpRequestCallback callback) throws IOException {
        this(port, "/", callback);
    }

    /**
     * Creates a new {@link TestHttpServer} instance for requests for a given path.
     * 
     * @param port the port number to listen to for incoming requests, &gt; 0;
     * @param path the path to listen for requests, cannot be <code>null</code> and should start with a forward slash (/);
     * @param callback the callback to invoke for each incoming request, cannot be <code>null</code>.
     * @throws IOException in case opening the server failed.
     */
    public TestHttpServer(int port, String path, HttpRequestCallback callback) throws IOException {
        m_port = port;
        m_server = HttpServer.create();
        m_server.setExecutor(Executors.newSingleThreadExecutor());
        m_server.createContext(path, new HttpHandler() {
            @Override
            public void handle(HttpExchange exchange) throws IOException {
                HttpResponse resp = callback.handle(new HttpRequest(exchange));
                if (resp != null) {
                    resp.writeTo(exchange);
                }
                else {
                    RESPONSE_NOT_FOUND.writeTo(exchange);
                }
            }
        });
    }

    /**
     * Starts the HTTP server on the defined port.
     * 
     * @throws IOException in case of problems binding to the defined port.
     */
    public final void start() throws IOException {
        m_server.bind(new InetSocketAddress(m_port), DEFAULT_BACKLOG);
        m_server.start();
    }

    /**
     * Stops the HTTP server immediately.
     */
    public final void stop() {
        m_server.stop(0);
    }
}
