package org.amdatu.security.oauth.itest;

import static org.amdatu.security.oauth.OAuthConstants.OAUTH_ACCESS_TYPE;
import static org.amdatu.security.oauth.OAuthConstants.OAUTH_API_KEY;
import static org.amdatu.security.oauth.OAuthConstants.OAUTH_API_PROVIDER;
import static org.amdatu.security.oauth.OAuthConstants.OAUTH_API_SECRET;
import static org.amdatu.security.oauth.OAuthConstants.OAUTH_API_VERSION;
import static org.amdatu.security.oauth.OAuthConstants.OAUTH_CALLBACK_URL;
import static org.amdatu.security.oauth.OAuthConstants.OAUTH_GRANT_TYPE;
import static org.amdatu.security.oauth.OAuthConstants.OAUTH_SCOPE;
import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createFactoryConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.*;
import static org.junit.Assume.*;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.atomic.AtomicReference;

import org.amdatu.security.oauth.itest.TestHttpServer.HttpRequest;
import org.amdatu.security.oauth.itest.TestHttpServer.HttpRequestCallback;
import org.amdatu.security.oauth.itest.TestHttpServer.HttpResponse;
import org.amdatu.testing.configurator.ConfigurationStep;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

public class OAuth2RoundTripTest {

    final class OAuth2Callback implements HttpRequestCallback {
        @Override
        public HttpResponse handle(HttpRequest req) {
            if ("GET".equals(req.getMethod()) && "/oauth2callback".equals(req.getPath())) {
                String code = req.getQueryParam("code");
                m_authCodeRef.set(code);
                return new HttpResponse(200, "Verified!");
            }
            return null;
        }
    }
    
    /** Provide your OAuth2 API key here for this test to work! */
    private static final String MY_API_KEY = "";
    /** Provide your OAuth2 API secret here for this test to work! */
    private static final String MY_API_SECRET = "";

    private final AtomicReference<String> m_authCodeRef = new AtomicReference<String>(null);

    // Injected by Amdatu Testing Configurator...
    private volatile OAuthService m_service;

    @Before
    public void setUp() throws Exception {
        assumeTrue("No Desktop API is available?!", Desktop.isDesktopSupported());
        assumeTrue("No API key and/or secret defined", !"".equals(MY_API_KEY) && !"".equals(MY_API_SECRET));

        configure(this)
            .add(new ConfigurationStep() {
                private volatile TestHttpServer m_httpd;

                @Override
                public void cleanUp(Object testCase) {
                    m_httpd.stop();
                }
                
                @Override
                public void apply(Object testCase) {
                    try {
                        m_httpd = new TestHttpServer(9080, new OAuth2Callback());
                        m_httpd.start();
                    }
                    catch (IOException e) {
                        fail(e.getMessage());
                    }
                }
            })
            .add(createFactoryConfiguration("org.amdatu.security.oauth.service.factory")
                .set(OAUTH_API_PROVIDER, "Google")
                .set(OAUTH_API_VERSION, "2")
                .set(OAUTH_API_KEY, MY_API_KEY)
                .set(OAUTH_API_SECRET, MY_API_SECRET)
                .set(OAUTH_SCOPE, "https://docs.google.com/feeds/")
                .set(OAUTH_GRANT_TYPE, "authorization_code")
                .set(OAUTH_ACCESS_TYPE, "offline")
                .set(OAUTH_CALLBACK_URL, "http://localhost:9080/oauth2callback"))
            .add(createServiceDependency().setService(OAuthService.class, "(&(api.provider=Google)(api.version=2))").setRequired(true))
            .apply();
    }

    @After
    public void tearDown() throws Exception {
        cleanUp(this);
    }

    @Test
    public void testRoundtripTestOk() throws Exception {
        Token requestToken = null; // not needed for Google

        String authUrl = m_service.getAuthorizationUrl(requestToken);
        assertNotNull("Failed to obtain authorization URL?!", authUrl);

        // Will open the current browser and callback on our own callback server...
        Desktop.getDesktop().browse(new URI(authUrl));

        String code = null;
        long start = System.currentTimeMillis();
        while (!Thread.currentThread().isInterrupted() && (System.currentTimeMillis() - start) < 60000L) {
            code = m_authCodeRef.get(); 
            if (code != null) {
                break;
            }
        }
        assertNotNull("Failed to obtain authorization code in time...!", code);

        Verifier v = new Verifier(code);
        Token accessToken = m_service.getAccessToken(requestToken, v);
        assertNotNull("Failed to obtain valid access token?!", accessToken);

        OAuthRequest request = new OAuthRequest(Verb.GET, "https://www.googleapis.com/drive/v2/about");
        m_service.signRequest(accessToken, request);

        Response response = request.send();

        assertTrue(response.isSuccessful());

        String body = response.getBody();
        assertTrue(body.contains("\"selfLink\": \"https://www.googleapis.com/drive/v2/about\""));
    }

}
