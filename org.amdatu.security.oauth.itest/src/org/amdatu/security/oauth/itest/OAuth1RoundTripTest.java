package org.amdatu.security.oauth.itest;

import static org.amdatu.security.oauth.OAuthConstants.*;
import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createFactoryConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.scribe.builder.api.Api;
import org.scribe.builder.api.DefaultApi10a;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

public class OAuth1RoundTripTest {
    private volatile OAuthService m_service;

    @Before
    public void setUp() throws Exception {
        BundleContext context = FrameworkUtil.getBundle(getClass()).getBundleContext();

        Api testApiProvider = new DefaultApi10a() {
            @Override
            public String getAccessTokenEndpoint() {
                return "http://oauthbin.com/v1/access-token";
            }

            @Override
            public Verb getAccessTokenVerb() {
                return Verb.GET;
            }

            @Override
            public String getAuthorizationUrl(Token t) {
                // This provider has already authorized the token, but we do it again...
                return "http://oauthbin.com/v1/request-token?oauth_token=" + t.getToken();
            }

            @Override
            public String getRequestTokenEndpoint() {
                return "http://oauthbin.com/v1/request-token";
            }

            @Override
            public Verb getRequestTokenVerb() {
                return Verb.GET;
            }
        };

        Properties props = new Properties();
        props.put(OAUTH_API_PROVIDER, "testProvider");
        props.put(OAUTH_API_VERSION, OAUTH_V1);

        context.registerService(Api.class.getName(), testApiProvider, props);

        configure(this)
            .add(createFactoryConfiguration("org.amdatu.security.oauth.service.factory")
                .set(OAUTH_API_PROVIDER, "testProvider")
                .set(OAUTH_API_KEY, "key")
                .set(OAUTH_API_SECRET, "secret"))
            .add(createServiceDependency().setService(OAuthService.class, "(api.provider=testProvider)").setRequired(true))
            .apply();

    }

    @After
    public void tearDown() {
        cleanUp(this);
    }

    @Test
    public void testRoundtripTestOk() throws Exception {
        String baseUrl = "http://oauthbin.com/v1/echo?";
        String input = "text=hello+world";

        Token requestToken = m_service.getRequestToken();
        assertNotNull("Failed to obtain valid request token?!", requestToken);

        String authUrl = m_service.getAuthorizationUrl(requestToken);
        assertNotNull("Failed to obtain authorization URL?!", authUrl);

        Verifier v = new Verifier("does not matter");
        Token accessToken = m_service.getAccessToken(requestToken, v);
        assertNotNull("Failed to obtain valid access token?!", accessToken);

        OAuthRequest request = new OAuthRequest(Verb.GET, baseUrl + input);
        m_service.signRequest(accessToken, request);

        Response response = request.send();

        assertTrue(response.isSuccessful());
        assertEquals(input, response.getBody());
    }
}
