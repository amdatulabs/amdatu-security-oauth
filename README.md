# Amdatu Security OAuth

Provides a configurable OAuth service for talking to external OAuth servers. Internally,
it uses [scribe-java](https://github.com/fernandezpablo85/scribe-java/) for the actual API
implementations. The main difference is that you can configure new OAuth servers by means
of supplying configurations through the ConfigAdmin service.

## Example usage

See the `org.amdatu.security.oauth.itest` project for two examples on how to use this
project. The `OAuth1RoundTripTest` shows how you can plug in new OAuth APIs, and the
`OAuth2RoundTripTest` shows how to use the Google Drive API using OAuth2. For this to
work, you need to supply a valid key and secret (see your [Google developers
console](https://console.developers.google.com)).

## Supported OAuth servers

By default several OAuth1 and OAuth2 APIs are supported. By registering your own API
implementation, you can support other OAuth servers as well.

### OAuth2 servers

The following OAuth v2 servers are supported:

* ConstantContact
* Facebook
* Foursquare
* Google
* Kaixin
* Live
* Renren
* SinaWeibo
* Viadeo
* Vkontakte

### OAuth1 servers

The following OAuth v1 servers are supported:

* AWeber
* ConstantContact
* Digg
* DropBox
* Evernote
* Flickr
* Foursquare
* Freelancer
* GetGlue
* Google
* ImgUr
* Kaixin
* LinkedIn
* LoveFilm
* Meetup
* Mendeley
* Miso
* NeteaseWeiboo
* NetProspex
* Plurk
* Px500
* QWeibo
* Sapo
* SimpleGeo
* SinaWeibo
* Skyrock
* SohuWeibo
* Trello
* Tumblr
* Twitter
* UbuntuOne
* Vimeo
* Xing
* Yahoo

## License

This project is licensed under Apache License v2.

